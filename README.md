Código fuente del proyecto en la carpeta pruebabeitech o en el repositorio remoto: https://bitbucket.org/jefersson_nava/pruebabeitechgrails/src/master/
Diagrama de flujo para crear una orden y añadir productos a una orden: Flujo crear orden.png

Desarrollado con el framework Grails 2.2.4 e Hibernate, seguridad con plugin apache-shiro
Se puede encontrar funcionando en: http://ec2-52-87-169-199.compute-1.amazonaws.com:8080  

Se ingresa con los siguientes usuarios: admin-admin, user2-user2, user3-user3

El usuario debe añadir un producto para crear una orden, mientras esté activa la orden los productos que añada el usuario se añaden a la misma orden.

Contiene módulo de seguridad para autenticar usuarios, cada usuario puede crear una orden añadiendo un producto, se permite una orden activa por usuario,
se debe finalizar una orden para poder crear una nueva.

Para despligue en IDE se debe configurar el framework Grails 2.2.4 y clonar el proyecto del repositorio o importarlo desde la carpeta
Solo es necesario crear la base de datos, al momento de desplegar el proyecto el pobla la información necesaria, usuarios, productos permitidos por usuario y permisos

Para despliegue en servidor tomcat7 se debe copiar el archivo grails-app-config.properties en la ruta de instalación de tomcat y en la carpeta lib, este archivo se puede editar
para configuración de base de datos mysql con los datos del entorno en que se despliegue, la base de datos debe estar creada para que sea exitoso el despligue. No 
es necesario poblarla con información. Utilizar el archivo ROOT.war adjunto y crear la base datos que se especifique en grails-app-config.properties.

Script para creación y poblado de información de base de datos: Dump20181130.sq  en este caso la base de datos se llama pruebabeitech
Documentación servicio REST en el archivo Serivico REST.pdf
Modelo entidad relación en formato imagen: MER.png
Archivo war para despliegue: ROOT.war





