package co.jefersson.prueba



import static org.springframework.http.HttpStatus.*

import org.springframework.stereotype.Service;

import grails.transaction.Transactional

/**
 * Clase que permite gestionar la comunicación entre la vista y el modelo para la funcionalidad de ordenes
 * @author Jefersson Nuñez
 * */
class OderController {
	
    static allowedMethods = [edit: ['GET', 'POST']]

    def index(Integer max) {
        model:[]
    }

	// Finaliza una orden
    def edit() {
		def orderInstance = Oder.findById(params.order as Long)
		if(orderInstance == null){
			render 'error@'
			render "No hay orden"
			return
		}
		
		orderInstance.active = 'NO'
		
		if(!orderInstance.save(flush:true)){
			render 'error@'
			render "No se pudo finalizar la orden"
			return
		}
		
		render 'success@'
		render "Exitoso, ya puede generar una nueva orden"
		return
    }

}
