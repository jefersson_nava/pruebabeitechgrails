package co.jefersson.prueba



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

/**
 * Clase que permite gestionar la comunicación entre la vista y el modelo para la funcionalidad de usuarios, solo lectura
 * @author Jefersson Nuñez
 * */
@Transactional(readOnly = true)
class UserController {

    def index(Integer max) {
        respond User.list(), model:[userInstanceCount: User.count()]
    }

}
