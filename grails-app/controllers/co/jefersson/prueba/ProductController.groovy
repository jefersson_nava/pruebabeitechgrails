package co.jefersson.prueba



import static org.springframework.http.HttpStatus.*

import grails.transaction.Transactional
import org.apache.shiro.SecurityUtils

/**
 * Clase que permite gestionar la comunicación entre la vista y el modelo para la funcionalidad de productos, solo lectura
 * @author Jefersson Nuñez
 * */
@Transactional(readOnly = true)
class ProductController {

	//Envía a la vista el listado de productos por usuario en sesión
    def index(Integer max) {
		def userInstance = User.findByUsername(SecurityUtils.subject.principal) // Usuario en sesión
		def listProducts = userInstance.products
		flash.message = message(code: 'product.info.message')
        model:[productInstanceList:listProducts]
    }

}
