package co.jefersson.prueba

import org.springframework.stereotype.Service;
import org.apache.shiro.SecurityUtils
import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.json.parser.JSONParser

/**
 * Controlador expuesto como servicio REST, habilitados métodos list y create
 * @author Jefersson Nuñez
 * */
@Service
class OrderRestController {
	
	def messageSource
	
	static allowedMethods = [list: "GET",create: ['GET', 'POST']]

	def create() {
		def userInstance = User.findByUsername(SecurityUtils.subject.principal)
		switch (request.method) {
			case 'GET':
				def orderInstance = Oder.findByCustomerAndActive(userInstance,'SI')
				if(orderInstance != null){
					render(contentType: 'text/json'){['status' : 'OK','message':'Existe una orden activa','deliveryAdress':orderInstance.deliveryAdress]}
					return
				}
				render(contentType: 'text/json'){['status' : 'ERROR','message':'No existe una orden activa']}
				return
			break
			case 'POST':
				def productInstance = Product.findById(params.product as Long)
				def error = []
				def messageSend = ""
				
				if(productInstance == null || params.quantity == null || params.deliveryAdress == null ){
					error << "Hace falta campos"
					render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo crear la orden','error':error]}
					return
				}
				
				if((productInstance.quantity - (params.quantity as Integer)) < 0){
					error << "No hay suficientes productos"
					render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo crear la orden','error':error]}
					return
				}
				
				if((params.quantity as Integer) == 0){
					error << "Debe ser al menos un producto"
					render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo crear la orden','error':error]}
					return
				}
				
				def orderInstance = Oder.findByCustomerAndActive(userInstance,'SI') // Buscar orden activa
				 
				if(orderInstance == null){
					orderInstance = new Oder()
					messageSend = "Se creó una nueva orden con este producto"
				}else{
					params.deliveryAdress = orderInstance.deliveryAdress
					messageSend = "Se adicionó el producto a la orden activa"
				}
				
				def priceOrder = orderInstance.price != null ? orderInstance.price : 0
				def quantityOrder = orderInstance.quantity != null ? orderInstance.quantity : 0
				
				def price = ((params.quantity as Integer)*productInstance.price) + priceOrder
				
				orderInstance.customer = userInstance
				orderInstance.price = price
				orderInstance.quantity = (params.quantity as Integer) + quantityOrder
				orderInstance.deliveryAdress = params.deliveryAdress
				
				// Guarda orden
				if (!orderInstance.save(flush: true)) {
					def locale = Locale.getDefault()
					orderInstance.errors?.allErrors?.each{
						String message = messageSource.getMessage(it, locale)
						error << message
					}

					render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo crear la orden','error':error]}
					return
				}

				//Asocia un detalle de orden por producto
				def orderDetailInstance = OrderDetail.findByOrderAndProduct(orderInstance,productInstance)
				
				if(orderDetailInstance == null){
					orderDetailInstance = new OrderDetail()
				}
				
				def priceOrderDetail = orderDetailInstance.price != null ? orderDetailInstance.price : 0
				def quantityOrderDetail = orderDetailInstance.quantity != null ? orderDetailInstance.quantity : 0
				
				orderDetailInstance.productDescription = productInstance.description
				orderDetailInstance.price = ((params.quantity as Integer)*productInstance.price) + priceOrderDetail
				orderDetailInstance.quantity = (params.quantity as Integer) + quantityOrderDetail
				orderDetailInstance.order = orderInstance
				orderDetailInstance.product = productInstance
				
				if (!orderDetailInstance.save(flush: true)) {
					render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo crear la orden']}
					return
				}
				
				//Reduce unidades disponibles por producto
				def quantityProduct = productInstance.quantity - (params.quantity as Integer)
				
				productInstance.quantity = quantityProduct
				productInstance.save(flush: true)
				
				render(contentType: 'text/json'){['status' : 'OK','message':messageSend,'quantityProduct':quantityProduct]}
				return
			break
		}
	}
	
	//Lista la ordenes de un usuario filtrando por fecha de creación, recibe fecha inicial y fecha final
	def list(){
		def error = []
		if(!params.dateIni){
			error << "No hay fecha inicial"
			render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo realizar la consulta','error':error]}
			return
		}
		if(!params.dateEnd){
			error << "No hay fecha final"
			render(contentType: 'text/json'){['status' : 'ERROR','message':'No se pudo realizar la consulta','error':error]}
			return
		}
		
		def dateIni = OrderUtils.formatStringToDate(params.dateIni)
		def dateEnd = OrderUtils.formatStringToDate(params.dateEnd)
		def userInstance = User.findByUsername(SecurityUtils.subject.principal)
		
		def orderInstanceList = Oder.findAll(){
			dateCreated >= dateIni && dateCreated <= dateEnd && customer == userInstance
		}
		
		def list = OrderUtils.formatOrderList(orderInstanceList)
		
		render(contentType: 'text/json'){['status' : 'OK','message':'Consulta realizada','orderInstanceList':list]}
		return
	}
	
}
