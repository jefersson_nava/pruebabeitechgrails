					<g:if test="${flash.message}">
						<div class="message" role="status">${flash.message}</div>
					</g:if>
					<g:if test="${userInstanceList.empty}">
						<p id="noEntries"><g:message code="default.noEntries.label"/></p>
					</g:if>
					<g:else>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center"><g:message code="user.username.label" /></th>
									<th class="text-center"><g:message code="user.name.label" /></th>
									<th class="text-center"><g:message code="user.lastName.label" /></th>
									<th class="text-center"><g:message code="user.products.label" /></th>
								</tr>
							</thead>
							<tbody>
							<g:each in="${userInstanceList}" status="i" var="userInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'} fil" data-id="${userInstance.id }">
									<td class="text-center">${fieldValue(bean: userInstance, field: "username")}</td>
									<td class="text-center">${fieldValue(bean: userInstance, field: "name")}</td>
									<td class="text-center">${fieldValue(bean: userInstance, field: "lastName")}</td>
									<td class="text-center">
										<g:each in="${userInstance?.products}" status="p" var="productInstance">
											${productInstance.name }
											<g:if test="${userInstance?.products?.size() - 1 != p }">
												-
											</g:if>
										</g:each>
									</td>
								</tr>
							</g:each>
							</tbody>
						</table>
						<div class="pagination mi-paginacion-ajax">
							<g:paginate total="${userInstanceCount ?: 0}" params="${params}"/>
						</div>
					</g:else>