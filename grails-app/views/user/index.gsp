
<%@ page import="co.jefersson.prueba.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row">
			<g:hiddenField id="urlBase" name="urlBase" value="${createLink(uri:'/')}" />
		    <div class="col-lg-12">
		        <div class="row">
		        	<div class="ibox float-e-margins">
	                     <div class="ibox-title">
	                         <h5><g:message code="default.users.label" /></h5>
	                     </div>  
						 <div class="ibox-content">
	                          <div class="feed-activity-list">
	                          	
	                                 <div class="row">
	                                 	
	                                 </div>
	                                 <br>
	                                 <div id="div-content-list-usuarios">
	                                 	<g:render template="listUsuarios" ></g:render>
	                                 </div>
	                                  
	                          </div>
	                     </div>
	                </div>
		        </div>
	        </div>
	       
		</div>
		
	</body>
</html>
