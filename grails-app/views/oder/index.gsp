
<%@ page import="co.jefersson.prueba.Oder" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'oder.label', default: 'Oder')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row">
			<g:hiddenField id="urlBase" name="urlBase" value="${createLink(uri:'/')}" />
		    <div class="col-lg-12">
		        <div class="row">
		        	<div class="ibox float-e-margins">
	                     <div class="ibox-title">
	                         <h5><g:message code="default.orders.label" /></h5>
	                     </div>  
						 <div class="ibox-content">
	                          <div class="feed-activity-list">
	                          	
	                                 <div class="row tooltip-demo">
	                                 	<div class="col-lg-8">
	                                 		<button class="btn btn-info btn-outline" type="button" id="btn-search"
	                                 			data-toggle="tooltip" title="Llama el método list del servicio rest, la petición ajax añade las fechas inicial y final de búsqueda" data-placement="right">
	                                 			Buscar
	                                 		</button>
	                                 	</div>
	                                 	<div class="container-fluid col-lg-4 pull-right">
	                                 		
	                                 	</div>
	                                 </div>
	                                 <br>
	                                 <div id="div-content-list-orders">
	                                 	
	                                 </div>
	                                  
	                          </div>
	                     </div>
	                </div>
		        </div>
	        </div>
	       
		</div>
		<asset:javascript src="order.js" />
	</body>
</html>
