					<g:if test="${flash.message}">
						<div class="message" role="status">${flash.message}</div>
					</g:if>
					<g:if test="${oderInstanceList.empty}">
						<p id="noEntries"><g:message code="default.noEntries.label"/></p>
					</g:if>
					<g:else>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center"><g:message code="oder.dateCreated.label" /></th>
									<th class="text-center"><g:message code="oder.id.label" /></th>
									<th class="text-center"><g:message code="oder.price.label" /></th>
									<th class="text-center"><g:message code="oder.deliveryAdress.label" /></th>
									<th class="text-center"><g:message code="oder.products.label" /></th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
							<g:each in="${oderInstanceList}" status="i" var="oderInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'} fil" data-id="${oderInstance.id }">
									<td><g:formatDate date="${oderInstance.dateCreated}" format="yyyy/MM/dd hh:mm a" /></td>
									<td class="text-center">${fieldValue(bean: oderInstance, field: "id")}</td>
									<td class="text-center"><g:formatNumber number="${oderInstance.price}" type="currency" currencyCode="COP" /></td>
									
									<td class="text-center">${fieldValue(bean: oderInstance, field: "deliveryAdress")}</td>
									<td class="text-center">
										<g:each in="${oderInstance.orderDetails}" status="j" var="oderDetailInstance">
											${ oderDetailInstance.quantity } x ${ oderDetailInstance.product.name } 
											<br>
										</g:each>
									</td>
									<td>
										<g:if test="${oderInstance.active == 'SI'}">
											<button class="btn btn-success btn-outline btn-end-order" 
												data-order="${oderInstance.id }" type="button" >
												Finalizar
											</button>
										</g:if>
										<g:else>
											<button class="btn btn-success btn-outline btn-end-order" disabled="disabled"
												data-order="${oderInstance.id }" type="button" >
												Finalizar
											</button>
										</g:else>
									</td>
								</tr>
							</g:each>
							</tbody>
						</table>
					</g:else>