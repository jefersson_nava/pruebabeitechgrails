
<%@ page import="co.jefersson.prueba.Product" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row">
			<g:hiddenField id="urlBase" name="urlBase" value="${createLink(uri:'/')}" />
		    <div class="col-lg-12">
		        <div class="row">
		        	<div class="ibox float-e-margins">
	                     <div class="ibox-title">
	                         <h5><g:message code="default.products.label" /></h5>
	                     </div>  
						 <div class="ibox-content">
	                          <div class="feed-activity-list">
	                          	
	                                 <div class="row">
	                                 	
	                                 </div>
	                                 <br>
	                                 <div id="div-content-list-products">
	                                 	<g:render template="listProducts" ></g:render>
	                                 </div>
	                                  
	                          </div>
	                     </div>
	                </div>
		        </div>
	        </div>
	       
		</div>
		
		<!-- Modal Agrega Producto -->
		<div class="modal inmodal" id="modal-add-product" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<form name="form-add-product" id="form-add-product" >
					<div class="modal-content">
				
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel"><g:message code="product.title.label"/></h4>
					</div>
					
					<div class="modal-body form-horizontal">
						
						<div id="div-form-product">
						
							<div class="form-group required" id="div-delivery-adress">
								<label for="nombre" class="col-sm-4 control-label field-required">
									<g:message code="order.deliveryAdress.label" default="Dirección" />
									<span class="text-danger">*</span>
								</label>
								<div class="col-sm-8">
									<g:textField name="deliveryAdress" id="deliveryAdress" class="input-sm col-xs-10 col-sm-10"  required="" />
								</div>
							</div>
							
							<div class="form-group required">
								<label for="formOrder" class="col-sm-4 control-label field-required">
									<g:message code="product.quantity.label"/>
									<span class="text-danger">*</span>
								</label>
								<div class="col-sm-8">
									<g:field type="number" min="1" id="quantity" name="quantity" class="input-sm col-xs-10 col-sm-10" required="" />
								</div>	
							</div>
							
						</div>
						
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white btn-modal-cerrar" data-dismiss="modal">
								<g:message code="default.cancel.label" />
							</button>
							<button type="submit" class="btn btn-primary btn-guardar-modal">
								<g:message code="default.add.product.label" />
							</button>
						</div>
					
					</div>
				</form>
			</div>
		</div>
		<asset:javascript src="product.js" />
	</body>
</html>
