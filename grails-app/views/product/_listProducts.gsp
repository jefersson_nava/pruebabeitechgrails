					<g:if test="${flash.message}">
						<div class="alert alert-info alert-dismissible div-info-product-order" role="alert">
							<button type="button" class="close" data-dismiss="alert">
								<span aria-hidden="true">&times;</span><span class="sr-only"><g:message
										code="default.button.close.label" /> </span>
							</button>
							${flash.message}
						</div>
					</g:if>
					<g:if test="${productInstanceList.empty}">
						<p id="noEntries"><g:message code="default.noEntries.label"/></p>
					</g:if>
					<g:else>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th class="text-center"><g:message code="product.name.label" /></th>
									<th class="text-center"><g:message code="product.price.label" /></th>
									<th class="text-center"><g:message code="product.quantity.label" /></th>
									<th class="text-center"><g:message code="prouct.description.label" /></th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
							<g:each in="${productInstanceList}" status="i" var="productInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'} fil" data-id="${productInstance.id }">
									<td class="text-center">${fieldValue(bean: productInstance, field: "name")}</td>
									<td class="text-center">${fieldValue(bean: productInstance, field: "price")}</td>
									<td class="text-center">${fieldValue(bean: productInstance, field: "quantity")}</td>
									<td class="text-center">${fieldValue(bean: productInstance, field: "description")}</td>
									<td class="text-center">
										<button class="btn btn-success btn-outline btn-add-product" data-quantity="${productInstance.quantity }"
											data-name="${productInstance.name }" data-product="${productInstance.id }" type="button" id="btn-add-product">
											<i class="fa fa-plus"></i>
										</button>
									</td>
								</tr>
							</g:each>
							</tbody>
						</table>
						<div class="pagination mi-paginacion-ajax">
							<g:paginate total="${productInstanceCount ?: 0}" params="${params}"/>
						</div>
					</g:else>