import co.jefersson.prueba.Product
import co.jefersson.prueba.Role
import co.jefersson.prueba.User
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->
		
		// Solo un rol en el sistema
		def roleInstanceList = Role.findAll()
		def superAdminRole= new Role(name: 'Super Administrador')
		if(roleInstanceList == null || roleInstanceList.isEmpty()){
			superAdminRole.addToPermissions('*:*')
			superAdminRole.save()
			println "superAdminRole: " +superAdminRole.errors
		}
		
		def listProductInstance = Product.findAll()
		def product1 = new Product(name:"Jabón Azul", price:1000,description:"Ideal para la ropa", quantity:3)
		def product2 = new Product(name:"Detergente", price:3000,description:"Deja suave la tela", quantity:5)
		def product3 = new Product(name:"Crema dental", price:8000,description:"Ideal para dientes blancos", quantity:2)
		def product4 = new Product(name:"Jabón Blanco", price:8000,description:"Ideal para la piel", quantity:4)
		def product5 = new Product(name:"Jabón Axión", price:8000,description:"El mejor arranca grasa", quantity:5)
		def product6 = new Product(name:"Clorox", price:500,description:"Desinfecta, agradable aroma", quantity:9)
		def product7 = new Product(name:"Jabón Rosa", price:2000,description:"Ideal para las manos", quantity:5)
		
		if(listProductInstance == null || listProductInstance.isEmpty()){
			product1.save()
			println "p1 " + product1.errors
			
			product2.save()
			println "p2 " + product2.errors
			
			product3.save()
			println "p3 " + product3.errors
			
			product4.save()
			println "p4 " + product4.errors
			
			product5.save()
			println "p5 " + product5.errors
			
			product6.save()
			println "p6 " + product6.errors
			
			product7.save()
			println "p7 " + product7.errors
		}
		
		def userInstanceList = User.findAll()
		if(userInstanceList == null || userInstanceList.isEmpty()){
			def user = new User(username: "admin", passwordHash: new Sha256Hash("admin").toHex(), name:"Jefersson", lastName:"Nava")
			user.addToRoles(superAdminRole)
			user.addToProducts(product1)
			user.addToProducts(product2)
			user.addToProducts(product7)
			user.save()
			println "user errors" + user.errors
			
			def user2 = new User(username: "user2", passwordHash: new Sha256Hash("user2").toHex(), name:"Juan", lastName:"Perez")
			user2.addToRoles(superAdminRole)
			user2.addToProducts(product3)
			user2.addToProducts(product2)
			user2.addToProducts(product4)
			user2.addToProducts(product5)
			user2.save()
			println "user2 errors" + user2.errors
			
			def user3 = new User(username: "user3", passwordHash: new Sha256Hash("user3").toHex(), name:"Marcos", lastName:"Suarez")
			user3.addToRoles(superAdminRole)
			user3.addToProducts(product6)
			user3.addToProducts(product2)
			user3.addToProducts(product4)
			user3.addToProducts(product5)
			user3.save()
			println "user3 errors" + user3.errors
		}
				
    }
    def destroy = {
    }
}
