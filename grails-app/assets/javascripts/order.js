var urlBase = $("#urlBase").val();

$(document).ready(function() {
	
	//---------------------------------------------------------------------------------------------------------------------------------
    // DETECCIÓN DEL BOTÓN FINALIZAR
    //---------------------------------------------------------------------------------------------------------------------------------
	$('body').on('click','.btn-end-order', function(event){
		var order = $(this).data('order');
		var button = $(this);
		//---------------------------------------
		$.ajax({
			type : 'POST',
			url : urlBase + 'oder/edit',
			data:{'order':order},
			success : function(data) {
				if(data.split('@')[0] == 'error'){
					alert('Error',data.split('@')[1],'error');
				}else{
					alert('Exitoso',data.split('@')[1],'success');
					button.attr('disabled','disabled');
				}
			},
			error : function() { // En caso de error en la petición
				alert('Error',"Ocurrió un error en la ejecución del proceso...",'error');
			}
		});
	});
	//---------------------------------------
	
	//---------------------------------------------------------------------------------------------------------------------------------
    // DETECCIÓN DEL BOTÓN BUSCAR
    //---------------------------------------------------------------------------------------------------------------------------------
	$("#btn-search").on('click',function(){
		var order = $(this).data('order');
		var button = $(this);
		var dateIni = subtractOneMonth() + ' 0:0';
		var dateEnd = convertDate() + ' 23:59';
		
		//---------------------------------------
		$.ajax({
			type : 'GET',
			url : urlBase + 'orderRest/list',
			data: {'dateIni':dateIni, 'dateEnd':dateEnd},
			success : function(data) {
				if(data.status == 'OK'){
					$("#div-content-list-orders").html('');
					buildTable(data.orderInstanceList);
				}else{
					var erro = data.error;
					for(var i = 0; i < erro.length; i++){
						alert('Error',erro[i],'error');
					}
				}
			},
			error : function() { // En caso de error en la petición
				alert('Error',"Ocurrió un error en la ejecución del proceso...",'error');
			}
		});
	});
	//---------------------------------------
		
});

function buildTable(data){
	
	var header = '<table class="table table-bordered table-striped">'+
		'<thead>'+
			'<tr>'+
				'<th class="text-center">Fecha Creación</th>'+
				'<th class="text-center">Identificador</th>'+
				'<th class="text-center">Total</th>'+
				'<th class="text-center">Dirección Envío</th>'+
				'<th class="text-center">Productos</th>'+
				'<th class="text-center"></th>'+
			'</tr>'+
		'</thead>'+
	'<tbody>';
	
	var rows = '';
	
	$.each(data, function(i, d) {
		var btnEnd = '';
		if(d['active'] == 'SI'){
			btnEnd = '<button class="btn btn-success btn-outline btn-end-order" '+
							'data-order='+d['id']+' type="button" >'+
							'Finalizar'+
					 '</button>';
		}else{
			btnEnd = '<button class="btn btn-success btn-outline btn-end-order" disabled="disabled" '+
							'data-order='+d['id']+' type="button" >'+
							'Finalizar'+
					 '</button>';
		}
		rows += '<tr class="fil" data-id="'+d['id']+'">'+
						'<td class="text-center">'+d['dateCreated']+'</td>'+
						'<td class="text-center">'+d['id']+'</td>'+
						'<td class="text-center">'+d['price']+'</td>'+
						'<td class="text-center">'+d['deliveryAdress']+'</td>'+
						'<td class="text-center">'+d['products']+'</td>'+
						'<td>'+btnEnd + '</td>'
					'</tr>';
				
		if(i == data.length -1){
			rows += '</tbody></table>' ;
		}
		
	});
	if(data.length >0){
		$("#div-content-list-orders").html(header + rows);
	}else{
		$("#div-content-list-orders").html('No hay elementos para mostrar');
	}
}

function subtractOneMonth() {
	  var d = new Date();
	  return [pad(d.getDate()), pad(d.getMonth()), d.getFullYear()].join('-');
}

function convertDate() {
	  var d = new Date();
	  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}

function pad(s) { return (s < 10) ? '0' + s : s; }

function alert(title,text,type  ){
	toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
    toastr[type](text, title);
}