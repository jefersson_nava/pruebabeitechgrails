var urlBase = $("#urlBase").val();
var idProduct = 0;

$(document).ready(function() {
	
	//---------------------------------------------------------------------------------------------------------------------------------
    // DETECCIÓN DEL BOTÓN ADD
    //---------------------------------------------------------------------------------------------------------------------------------
	$(".btn-add-product").on('click',function(){
		idProduct = $(this).data('product');
		var quantity = $(this).data('quantity');
		var name = $(this).data('name');
		var h4 = $(".modal-header h4").html(name);
		
		//Verificar si existe orden activa
		//---------------------------------------
		$.ajax({
			type : 'GET',
			url : urlBase + 'orderRest/create',
			success : function(data) {
					
				if(data != null){
					$("#quantity").attr('max',quantity);
					if(data.status == 'OK'){
						//Existe orden activa
						$("#deliveryAdress").attr('type',true);
						$("#div-delivery-adress").attr('style','display:none');
						$("#deliveryAdress").val(data.deliveryAdress);
						$('#modal-add-product').modal('show');
					}else{
						//Crear orden nueva
						$("#div-delivery-adress").attr('style','display:block');
						$('#modal-add-product').modal('show');
					}
				}else{
					alert('Error',"Ocurrió un error en la ejecución del proceso...",'error');
				}
			},
			error : function() { // En caso de error en la petición
				alert('Error',"Ocurrió un error en la ejecución del proceso...",'error');
			}
		});
		//---------------------------------------
		
	});
	
	//DETECCIÓN DEL FORMULARIO AÑADIR PRODUCTO
    //---------------------------------------------------------------------------------------------------------------------------------
	$("form[name='form-add-product']").submit(function(event){
		$.ajax({
			type:'POST',
			url : urlBase + 'orderRest/create',
			data : $(this).serialize() + "&product="+idProduct,
			success : function(data) {
				if(data.status == 'OK'){
					alert('Exitoso',data.message,'success');
					$('#modal-add-product').modal('hide');
					
					minusProduct(data.quantityProduct);//Disminuir producto
				}else{
					var erro = data.error;
					for(var i = 0; i < erro.length; i++){
						alert('Error',erro[i],'error');
					}
				}
			},
			error : function() { // En caso de error en la petición
				alert('Error',"Ocurrió un error en la ejecución del proceso...",'error');
			}
		});
		event.preventDefault();
		return false;
	});
    //---------------------------------------------------------------------------------------------------------------------------------
	
	/* BORRAR FORMULARIOS DE MODAL CUANDO SE CIERRA */
    /**********************************************************************************************************************************/
    $('#modal-add-product').on('hidden.bs.modal', function (e) {
		$('#quantity').val('');
		$('#deliveryAdress').val('');
	});
    
    //Cerrar mensaje informativo
    setTimeout(function(){ $('.div-info-product-order').html('').removeAttr("class"); }, 10000);
	
});

//Disminuye la cantidad del producto
function minusProduct(quantityProduct){
	$('.fil').each(function(index,fila){
		var id = $(this).data('id');
		var cellTwo = $(this).find("td").eq(2);
		var cellFour = $(this).find("td").eq(4);
		if(id == idProduct){
			cellTwo.text(quantityProduct)
			var button = cellFour.find("button");
			button.data('quantity',quantityProduct);
		}
	});
}


function alert(title,text,type  ){
	toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
    toastr[type](text, title);
}