// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//

$(document).ajaxComplete(function (event, xhr, settings) {  
	if( xhr.responseText == 'Sesión Caducada' ){
		location.reload();
	}
});