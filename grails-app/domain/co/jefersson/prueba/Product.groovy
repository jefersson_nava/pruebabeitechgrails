package co.jefersson.prueba

import java.util.Date;

/**
 * Clase de dominio que mapea la tabla product y se utiliza para ingreso de nuevos registros
 * @author Jefersson Nuñez
 * */
class Product {
	
	String name
	Integer price
	String description
	Integer quantity
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [ users: User ]
	static belongsTo = User

    static constraints = {
		name(nullable: false, blank: false, unique: true)
		price(nullable: false, blank: false)
		quantity(nullable: false, blank: false)
		description(nullable:true)
    }
	
	static mapping = {
		autoTimestamp true // Permite generar fechas automáticamente
		sort id: 'asc'
	}
}
