package co.jefersson.prueba

/**
 * Clase de dominio que mapea la tabla role y se utiliza para ingreso de nuevos registros
 * @author Jefersson Nuñez
 * */
class Role {
    String name

    static hasMany = [ users: User, permissions: String ]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false, unique: true)
    }
}
