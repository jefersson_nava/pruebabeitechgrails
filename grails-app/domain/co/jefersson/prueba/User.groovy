package co.jefersson.prueba

/**
 * Clase de dominio que mapea la tabla customer y se utiliza para ingreso de nuevos registros
 * @author Jefersson Nuñez
 * */
class User {
	String name
	String lastName
    String username
    String passwordHash
	
	Date dateCreated
	Date lastUpdated
    
    static hasMany = [ roles: Role, permissions: String, products:Product ] // Relaciones
	
	// Restricciones
    static constraints = {
        username(nullable: false, blank: false, unique: true)
		name(nullable: false, blank: false)
		lastName(nullable: false, blank: false)
		products nullable : true
    }
	
	static mapping = {
		version false
		table 'customer'
		id generator:'native'
		autoTimestamp true // Permite generar fechas automáticamente
	}
}
