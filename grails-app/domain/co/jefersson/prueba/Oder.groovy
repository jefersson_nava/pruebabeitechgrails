package co.jefersson.prueba

import java.util.Date;
import grails.validation.Validateable

/**
 * Clase de dominio que mapea la tabla order y se utiliza para ingreso de nuevos registros
 * @author Jefersson Nuñez
 * */
@Validateable
class Oder {

	String deliveryAdress
	Integer price
	Integer quantity
	String active = 'SI' // Un usuario solo puede tener una orden activa a la vez
	
    Date dateCreated
	Date lastUpdated
	
	User customer
	
	static hasMany = [ orderDetails: OrderDetail ]
	
    static constraints = {
		deliveryAdress(nullable: false, blank: false)
		quantity max:5 // Se limita el número de pedidos en una orden por usuario
    }
	
	static mapping = {
		autoTimestamp true // Permite generar fechas automáticamente
		sort id: 'asc'
	}
}
