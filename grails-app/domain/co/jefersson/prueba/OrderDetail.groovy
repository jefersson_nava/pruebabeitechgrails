package co.jefersson.prueba

import java.util.Date;

/**
 * Clase de dominio que mapea la tabla order_detail y se utiliza para ingreso de nuevos registros
 * @author Jefersson Nuñez
 * */
class OrderDetail {
	
	String productDescription
	Integer price
	Integer quantity
	
	Oder order
	Product product
	
	Date dateCreated
	Date lastUpdated

    static constraints = {
    }
	
	static mapping = {
		autoTimestamp true // Permite generar fechas automáticamente
	}
}
