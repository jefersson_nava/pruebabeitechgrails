package co.jefersson.prueba;

import org.codehaus.groovy.grails.web.json.JSONArray

/**
 * Clase que permite generar dar formatos a fechas y construye el json a enviar en el método list del servicio REST
 * @author Jefersson Nuñez
 * */
public class OrderUtils {

	public OrderUtils(){}
	
	// Arma el JSON a enviar
	def static formatOrderList(oderInstanceList){
		
		HashMap jsonMap = new HashMap()
		JSONArray list = new JSONArray()
		oderInstanceList.each{item->
			
			jsonMap.id = item?.id
			jsonMap.dateCreated = formatDateToString(item?.dateCreated)
			jsonMap.price = 'COP '+ item?.price
			jsonMap.deliveryAdress = item?.deliveryAdress
			jsonMap.products = products(item?.orderDetails)
			jsonMap.active = item?.active
			
			HashMap jsonGeneral = new HashMap(jsonMap)
			list.add(jsonGeneral)
			
		}
		return list
	}
	
	//Retorna string de la columna productos
	def static products(orderDetails){
		String produc = ""
		if(orderDetails != null){
			orderDetails.each{detail ->
				produc += detail.quantity + ' x ' +detail.product.name + ' <br>'
			}
		}
		return produc
	}
	
	//Retorna fecha en formato string
	def static formatDateToString(oldDate){
		return oldDate != null ? oldDate.format( 'yyyy-MM-dd HH:mm' ) : ""
	}
	
	//Retorna fecha string en formato Date
	def static formatStringToDate(oldDate){
		def date = new Date().parse("dd-MM-yyyy HH:mm", oldDate)
		return date
	}
}
